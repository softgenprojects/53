const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

async function createICO(data) {
  try {
    const ico = await prisma.iCO.create({
      data: data,
    });
    return ico;
  } catch (error) {
    console.error('Error creating ICO:', error);
    throw error;
  }
}

async function getAllICOs() {
  try {
    const icos = await prisma.iCO.findMany();
    return icos;
  } catch (error) {
    console.error('Error retrieving ICOs:', error);
    throw error;
  }
}

async function getICOById(id) {
  try {
    const ico = await prisma.iCO.findUnique({
      where: { id: parseInt(id) },
    });
    return ico;
  } catch (error) {
    console.error('Error retrieving ICO by ID:', error);
    throw error;
  }
}

async function investInICO(icoId, amount) {
  try {
    const investment = await prisma.investment.create({
      data: {
        icoId: parseInt(icoId),
        amount: parseFloat(amount),
      },
    });
    return investment;
  } catch (error) {
    console.error('Error creating investment:', error);
    throw error;
  }
}

async function deleteICO(id) {
  try {
    const ico = await prisma.iCO.delete({
      where: { id: parseInt(id) },
    });
    return ico;
  } catch (error) {
    console.error('Error deleting ICO:', error);
    throw error;
  }
}

module.exports = { createICO, getAllICOs, getICOById, investInICO, deleteICO };