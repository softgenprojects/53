const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const createICO = async ({ name, description, startTime, endTime, tokenName, tokenSymbol, totalTokens, tokensPerEth, status }) => {
  try {
    const ico = await prisma.iCO.create({
      data: {
        name,
        description,
        startTime,
        endTime,
        tokenName,
        tokenSymbol,
        totalTokens,
        tokensPerEth,
        status
      }
    });
    return ico;
  } catch (error) {
    throw error;
  }
};

const getICOById = async (id) => {
  try {
    const ico = await prisma.iCO.findUnique({
      where: { id }
    });
    return ico;
  } catch (error) {
    throw error;
  }
};

const getAllICOs = async () => {
  try {
    const icos = await prisma.iCO.findMany();
    return icos;
  } catch (error) {
    throw error;
  }
};

const investInICO = async ({ investorId, icoId, amount }) => {
  try {
    const investment = await prisma.investor.update({
      where: { id: investorId },
      data: {
        investedAmount: {
          increment: amount
        },
        icos: {
          connect: { id: icoId }
        }
      }
    });
    await prisma.iCO.update({
      where: { id: icoId },
      data: {
        totalTokens: {
          decrement: amount
        }
      }
    });
    return investment;
  } catch (error) {
    throw error;
  }
};

const deleteICO = async (id) => {
  try {
    const ico = await prisma.iCO.delete({
      where: { id }
    });
    return ico;
  } catch (error) {
    throw error;
  }
};

module.exports = { createICO, getICOById, getAllICOs, investInICO, deleteICO };