const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const { body, validationResult } = require('express-validator');
const ICOServices = require('../services/ICOServices');

router.post('/create', [
  body('name').notEmpty().withMessage('Name is required'),
  body('symbol').notEmpty().withMessage('Symbol is required'),
  body('totalSupply').isNumeric().withMessage('Total supply must be a number'),
  body('price').isNumeric().withMessage('Price must be a number')
], asyncHandler(async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  try {
    const ico = await ICOServices.createICO(req.body);
    res.status(201).json(ico);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
}));

router.post('/invest', [
  body('icoId').notEmpty().withMessage('ICO ID is required'),
  body('amount').isNumeric().withMessage('Amount must be a number')
], asyncHandler(async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  try {
    const investment = await ICOServices.investInICO(req.body);
    res.status(200).json(investment);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
}));

router.get('/ico/:id', asyncHandler(async (req, res) => {
  try {
    const ico = await ICOServices.getICOById(req.params.id);
    if (!ico) {
      return res.status(404).json({ message: 'ICO not found' });
    }
    res.status(200).json(ico);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
}));

router.get('/icos', asyncHandler(async (req, res) => {
  try {
    const icos = await ICOServices.getAllICOs();
    res.status(200).json(icos);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
}));

router.delete('/ico/:id', asyncHandler(async (req, res) => {
  try {
    const result = await ICOServices.deleteICO(req.params.id);
    if (!result) {
      return res.status(404).json({ message: 'ICO not found or already deleted' });
    }
    res.status(204).send();
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
}));

module.exports = router;