const express = require('express');
const router = express.Router();
const ICOServices = require('@services/ICOServices');

router.post('/create', async (req, res) => {
  try {
    const { name, description, startTime, endTime, tokenName, tokenSymbol, totalTokens, tokensPerEth, status } = req.body;
    const ico = await ICOServices.createICO({ name, description, startTime, endTime, tokenName, tokenSymbol, totalTokens, tokensPerEth, status });
    res.status(201).json(ico);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

router.get('/icos', async (req, res) => {
  try {
    const icos = await ICOServices.getAllICOs();
    res.status(200).json(icos);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

router.post('/invest', async (req, res) => {
  try {
    const { investorId, icoId, amount } = req.body;
    const investment = await ICOServices.investInICO({ investorId, icoId, amount });
    res.status(200).json(investment);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

router.get('/ico/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const ico = await ICOServices.getICOById(id);
    if (!ico) {
      return res.status(404).json({ message: 'ICO not found' });
    }
    res.status(200).json(ico);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

router.delete('/ico/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const ico = await ICOServices.deleteICO(id);
    res.status(200).json(ico);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

module.exports = router;