const express = require('express');
const ICOController = require('../controllers/ICOController');

const router = express.Router();

router.post('/create', ICOController.createICO);
router.post('/invest', ICOController.investInICO);
router.get('/ico/:id', ICOController.getICOById);
router.get('/icos', ICOController.getAllICOs);
router.delete('/ico/:id', ICOController.deleteICO);

module.exports = router;